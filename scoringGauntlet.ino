#include <Arduino.h>
#include "SimpleMCP23008.h"

SimpleMCP23008 switchLeft(0x27);
SimpleMCP23008 switchRight(0x20);

#ifdef ARDUINO_attiny
    #include <SoftwareSerial.h>
    #define RX 1
    #define TX 4 // pin 3, Arduino digital IO 4
    SoftwareSerial Serial(RX, TX);
#endif
/* I would like to thank https://tronixstuff.com/2011/08/26/arduino-mcp23017-tutorial/
 * not so much for implementation as illustrating that I was actually on track.
 *
 * After much buying, waiting to arrival, coming up with new idea and buying more stuff...
 *
 * use "easy to press" switches when the finger is DOWN. So we need to invert the LEDs
 *
 * 2020-12-29: I've gotten sick of trying to integrate the ADXL345s properly. Besides which they create a risk
 * factor for jumping / falling over etc. Replace them with a "lead declared" button that tracks when the finger
 * is up (i.e. not being pressed) and a "send score" button to replace the "mute" button.
 *
 * 2021-02-14: For the love of fuck, make sure Rx pullups on the ESP is DISABLED
 *
 * 2021-11-10: Replacing tactile switches with MX keys inside a small enclosure with flanges that allow it to be velcro'd to the hand
 * As a result, MCP23008 0-3 are switches, 4-7 are their corresponding LEDs, with the buzzer for the RHS on pin 4 (i.e. LED 0)
 */

long lastSerial;
long buzzTime;

#define MAX_LINE 20
char line[MAX_LINE]={0};
uint8_t lineIndex;
signed char buzzCount;

long blinkTime;

#define BLINK_TIME 250
#define SIGH_TIME 1500
#define PRINT_TIME 250

#define BIT_BUZZER 0x10
#define BITMASK_SWITCH 0x0F

#define LEFT_SWITCH_MASK 0x0F
#define RIGHT_SWITCH_MASK 0x0F

void setup() {
    Serial.begin(38400);
    Serial.println("# Setup");
    TheWire.begin();

    Serial.println("#  GPIO:");
    switchLeft.setDDR(LEFT_SWITCH_MASK);
    switchRight.setDDR(RIGHT_SWITCH_MASK);
    Serial.println("#  DDR set");

    // We ignore inversion as a "business logic" concern. Not to mention my current intention is to
    // provide a command line option to indicate whether the bank of switches is being held "upside down"
    switchLeft.setInversion(0x00);
    switchRight.setInversion(0x00);
    Serial.println("#  Inversion set");

    switchLeft.setPullUps(LEFT_SWITCH_MASK);
    switchRight.setPullUps(RIGHT_SWITCH_MASK);
    Serial.println("#  Pullups set");

    switchLeft.write(0xF0);
    // turn all the scoring LEDs on
    switchRight.write(0x00); // ~BIT_BUZZER)
    Serial.println("# IOExpanders done");

    lastSerial = 0;
    buzzTime = 0;
    lineIndex = 0;
    buzzCount = -1;
    memset(line, 0, 10);
    Serial.println("# Setup done");
}

void loop() {
    long currentTimeMillis = millis();
    static bool state = false;

    unsigned char scoreFingers = switchLeft.read();
    unsigned char leadFingers = switchRight.read();

    // make the score LEDs match the fingers:
    switchLeft.write((scoreFingers<<4) & 0xF0);

    // are we in the middle of buzzing?
    unsigned char leadIO = 0x00;
    if (buzzCount >= 0) {
        // yes, is it time to toggle?
        if (currentTimeMillis - buzzTime > blinkTime) {
            buzzTime = currentTimeMillis;
            buzzCount--;
        }
        // Note well: if we don't do the bit below, we just mirror what was there before and it all goes to hell
        if ((buzzCount % 2) > 0) {
            leadIO &= ~BIT_BUZZER; // clear bit 4 (turning the buzzer off)
        } else {
            leadIO |= BIT_BUZZER; // set bit 4 (turning the buzzer on)
        }
    } else {
        leadIO &= ~BIT_BUZZER;
    }
    switchRight.write(leadIO);

    // every 1/4 of a second report the fingers
    if (currentTimeMillis - lastSerial > PRINT_TIME) {
        scoreFingers &= LEFT_SWITCH_MASK;
        Serial.print(scoreFingers, BIN);

        Serial.print(" ");
        
        leadFingers &= RIGHT_SWITCH_MASK;
        Serial.println(leadFingers, BIN);

        lastSerial = millis();
    }

#ifdef ARDUINO_attiny
    // note well: if you take your time you may bork the datastream:
    serialEvent(); // do not make this call unless you're using a SoftwareSerial.
#endif
    return;
}

void serialEvent() {
    while (Serial.available()) {
        char c = (char) Serial.read();
        switch (c) {
            case '\n':
                if (lineIndex <= 0) {
                    break;
                }
                switch(line[0]) {
                    case 'b':
                        // how many times to "buzz blink"
                        if (lineIndex <= 1) {
                            break;
                        }
                        buzzCount = line[1] - '0';
                        Serial.println("# Buzz " + String(buzzCount));
                        if (buzzCount == 0) {
                            buzzCount = 1;
                            blinkTime = SIGH_TIME;
                        } else {
                            buzzCount *= 2;
                            blinkTime = BLINK_TIME;
                        }
                        buzzTime = 0;
                        break;
                    case 'w':
                    case 'W':
                        // whatareyou
                        Serial.println("! GAUNTLET");
                        break;
                }
                for (int i=0; i <= lineIndex; i++) {
                    line[i] = 0;
                }
                lineIndex = 0;
                break;
            case '\r':
                break;
            default:
                line[lineIndex++] = c;
                if (lineIndex >= MAX_LINE) {
                    lineIndex = 0;
                }
                break;
        }
    }
    return;
}
